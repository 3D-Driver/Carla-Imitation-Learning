# Midterm-Presentation

- [ ] Configure powerful PC, code for convert depth and simulate lidar (Dai)
- [ ] confirm what measurements/inputs are used in CIL and how to crop image (Wang)
- [ ] confirm outputs (steer, throttle, bounding box), calculate the mean (Huang)
- [ ] confirm network architecture, measurements, loss function, and how to augment (Huang)
- [ ] create a small dataset including depth, lidar, bounding box, action, measurements, stereo (Wang)
- [ ] videos (CIL, autopilot, random, ours) 

# Update:
1. Run run_cil.py directly.
2. Set Cameras in carla.benchmarks.corl_2017.py.
3. Collect data in imitation_learning.py's run_step() function (for training offline ?)
4. Change network architectures in imitation_learning_network.py.
5. Load pre-trained model in imitation_learning.py (only load those variables in the pre-trained model as our network are changing).
6. Data Augmentation using imgaug library like [here](https://github.com/carla-simulator/imitation-learning/issues/1)
7. Train branches using mask (only one branch activated) like [here](https://github.com/carla-simulator/imitation-learning/issues/1)
8. Run python run_CIL.py -c Town02 for evaluation and default (Town01) for collecting data.

# [Midterm] What should we present --2018.4.10
- Two videos: Huang Xingchang
    + run_CIL (given example)
    + Add 3D-bounding box 
        * offline
- Dataset for trainning: Dai Qi
    + Point cloud data
    + Depth
    + RGB 
    + High-level command
    + measurement: speed
- Presentation: Wang Haoran
    + Comparison: Check Schedule 