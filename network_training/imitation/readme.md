## How to train our model on your own dataset 
We provide the scripts about network training on our own created datasets from ```./data_preparation``` folder.

### Prerequisite
* Python 3.6.4
* Tensorflow 1.8.0
* Imgaug Library

### Running Steps
1. Put all the ".pkl" training data in ```"../../data/_out"``` directory.
2. Move part of training data to ```"../../data/_test"``` directory.
3. Put pretrained model in ```./model``` directory.
3. GPU: ```python imitation_learning.py```
4. CPU: ```python local_imitation_learning.py```
