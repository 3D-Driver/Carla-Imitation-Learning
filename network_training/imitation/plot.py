from __future__ import print_function

import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import scipy
import glob
import pickle
import tensorflow as tf
import numpy as np
from carla import carla_server_pb2 as carla_protocol
from carla.transform import Transform
import math
from numpy.linalg import pinv, inv
import random
import colorsys
import cv2

slim = tf.contrib.slim

from imgaug import augmenters as iaa
from collections import defaultdict
from carla.image_converter import depth_to_array, to_rgb_array, depth_to_logarithmic_grayscale

fig, ax = plt.subplots(1)

def plot(log_file, label):
	losses = []
	with open(log_file, 'rb') as f:
		content = f.readlines()
		content = [str(x, encoding = "utf-8").strip('\n').split(' ') for x in content]
		print (len(content))
		for line in content:
			if line[-3] == 'test'or line[-4] == 'Restoring':
				continue
			else:
				losses.append(float(line[-1])*1000)


	print (losses)
	y = range(1,21)
	ax.plot(y,losses,label=label)
	# ax.legend(loc='upper right')


def plot_depth(log_file, label):
    losses = []
    with open(log_file, 'rb') as f:
        content = f.readlines()
        content = [str(x, encoding = "utf-8").strip('\n').split(' ') for x in content]
        print (len(content))
        flag = 0
        for line in content:
#            print (line[-1])
            if flag and line[-3] != 'test' and line[-1] != 'last_test_cost' and line[-4] != 'Restoring':
#                print (line[-1])
                losses.append(float(line[-1])*1000)
            if line[-3] == 'test':
                flag = 1
            if line[-1] == 'last_test_cost' or line[-4] == 'Restoring':
                flag = 0


    print (len(losses))
    y = range(1, len(losses)+1)
    ax.plot(y,losses,label=label)


def main():
#    plot('out_rgb_small_bn.log', 'RGB')
#    plot('out_depth.log', 'RGB-D')
#    plot('out_lidar.log', 'RGB-D(Sparse)')

    plot_depth('test_depth_bn.log', 'RGB-D')
    # ax.set_xlim((3.5, 4.5))
    ax.set_title('Validation losses')
    ax.set_ylim((4.5, 6.5))
    ax.set_xlabel("Iterations",fontsize=12)
    ax.set_ylabel("Weighted loss/1e-3",fontsize=12)
    ax.legend()
    plt.show()


if __name__ == '__main__':
	main()








