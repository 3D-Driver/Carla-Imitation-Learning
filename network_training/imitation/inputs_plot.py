from __future__ import print_function

import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import scipy
import glob
import pickle
import tensorflow as tf
import numpy as np
from carla import carla_server_pb2 as carla_protocol
from carla.transform import Transform
import math
from numpy.linalg import pinv, inv
import random
import colorsys
import cv2

slim = tf.contrib.slim

from imgaug import augmenters as iaa
from collections import defaultdict
from carla.image_converter import depth_to_array, to_rgb_array, depth_to_logarithmic_grayscale


# https://stackoverflow.com/questions/35164529/in-tensorflow-is-there-any-way-to-just-initialize-uninitialised-variables/43601894#43601894

st = lambda aug: iaa.Sometimes(0.4, aug)
oc = lambda aug: iaa.Sometimes(0.3, aug)
rl = lambda aug: iaa.Sometimes(0.09, aug)
augment = iaa.Sequential([
	rl(iaa.GaussianBlur((0, 1.5))), # blur images with a sigma between 0 and 1.5
	rl(iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05), per_channel=0.5)), # add gaussian noise to images
	oc(iaa.Dropout((0.0, 0.10), per_channel=0.5)), # randomly remove up to X% of the pixels
	oc(iaa.CoarseDropout((0.0, 0.10), size_percent=(0.08, 0.2),per_channel=0.5)), # randomly remove up to X% of the pixels
	oc(iaa.Add((-40, 40), per_channel=0.5)), # change brightness of images (by -X to Y of original value)
	st(iaa.Multiply((0.10, 2.5), per_channel=0.2)), # change brightness of images (X-Y% of original value)
	rl(iaa.ContrastNormalization((0.5, 1.5), per_channel=0.5)), # improve or worsen the contrast
	rl(iaa.Grayscale((0.0, 1))), # put grayscale
], random_order=True) # do all of the above in random order
# from carla.benchmarks.agent import Agent
# from carla.carla_server_pb2 import Control

from imitation_learning_network import load_imitation_learning_network
import logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename='out.log',
                    filemode='w')
logger = logging.getLogger(__name__)


FRAMES = 3
ITERS = 1
EPOCHS = 20
BATCH_SIZE = 2
DIRECTIONS = [2,3,4,5]

training_cost = []
testing_cost = []

WINDOW_WIDTH = 200
WINDOW_HEIGHT = 160

WINDOW_WIDTH_HALF = WINDOW_WIDTH / 2
WINDOW_HEIGHT_HALF = WINDOW_HEIGHT / 2

k = np.identity(3)
k[0, 2] = WINDOW_WIDTH_HALF
k[1, 2] = WINDOW_HEIGHT_HALF
k[0, 0] = k[1, 1] = WINDOW_WIDTH / (2.0 * math.tan(100.0 * math.pi / 360.0))

def rand_color(seed):
    """Return random color based on a seed"""
    random.seed(seed)
    col = colorsys.hls_to_rgb(random.random(), random.uniform(.2, .8), 1.0)
    return (int(col[0]*255), int(col[1]*255), int(col[2]*255))


fig,ax = plt.subplots(1)

def draw_rect(x_2d, y_2d, rgb_image):

	# # Display the image
	ax.imshow(rgb_image)
	# Create a Rectangle patch
	rect = patches.Rectangle((x_2d,y_2d),1,1,linewidth=1,edgecolor='r',facecolor='none')
	# # Add the patch to the Axes
	ax.add_patch(rect)
	# print ('draw')
	

def get_camera_params():
	with open('camera_params.pkl', 'rb') as input:
		x = pickle.load(input)
		camera_to_car_transform = x['camera0_params']
		lidar_to_car_transform = x['lidar_params']
	return camera_to_car_transform, lidar_to_car_transform
# camera_to_car_transform = camera0.get_unreal_transform()
# lidar_to_car_transform = lidar.get_unreal_transform()

def get_lidar(measurements, camera_to_car_transform, lidar_to_car_transform, rgb_image, lidar_data):
	world_transform = Transform(measurements.player_measurements.transform)
	extrinsic = world_transform * lidar_to_car_transform
	
	lidar_image = np.ones((WINDOW_HEIGHT, WINDOW_WIDTH))

	print (lidar_data.shape)
	for i in range(lidar_data.shape[0]):
	# for i in range(1000):
		pos_vector = np.array([ [lidar_data[i,0]], [lidar_data[i,1]], [lidar_data[i,2]], [1.0]]) # homo.
		# print (pos_vector)
		if pos_vector[1] < 0:
			point_pos = np.dot(lidar_to_car_transform.matrix, pos_vector)
			# print (point_pos)
			point_pos = np.dot(inv(camera_to_car_transform.matrix), point_pos)
			# print (point_pos)
			pos2d = np.dot(k, point_pos[:3])
			pos2d = np.array([pos2d[0] / pos2d[2], pos2d[1] / pos2d[2], pos2d[2]])
			# print (pos2d)
			x_2d = int(WINDOW_WIDTH - pos2d[0])
			y_2d = int(WINDOW_HEIGHT - pos2d[1])
			# print (x_2d, y_2d)
			if (x_2d >= 0 and x_2d < WINDOW_WIDTH and y_2d >= 0 and y_2d < WINDOW_HEIGHT):
				# print (x_2d, y_2d)
				# print (-pos_vector[1])
				# lidar_image[y_2d, x_2d] = np.sqrt(np.sum(pos_vector**2)) / 1000
				draw_rect(x_2d, y_2d, rgb_image)
	
	# plt.imshow(lidar_image)
	plt.show()
	return lidar_image

def get_bounding_box(measurements, camera_to_car_transform, lidar_to_car_transform, rgb_image):
	world_transform = Transform(measurements.player_measurements.transform)

	# print (measurements.player_measurements.transform)
	# print (type(camera_to_car_transform), type(world_transform))
	extrinsic = world_transform * camera_to_car_transform
	# print (type(extrinsic))
	bbox_image = np.ones((WINDOW_HEIGHT, WINDOW_WIDTH))
	for agent in measurements.non_player_agents:
		if agent.HasField('vehicle'):
			# print ('vehicle')
			pos = agent.vehicle.transform.location
			pos_vector = np.array([[pos.x], [pos.y], [pos.z], [1.0]]) # homo.
			# print (pos_vector)
			# camera to world Rt matrix
			Rt = Transform(agent.vehicle.transform)
			loc = np.dot(inv(Rt.matrix), pos_vector)
			bbox = agent.vehicle.box_extent
			bbox_vector = np.array([[bbox.x], [bbox.y], [bbox.z], [0.0]])

			mask = np.array([[[1],[1],[1],[0]],[[1],[1],[-1],[0]],[[1],[-1],[-1],[0]],[[1],[-1],[1],[0]],[[-1],[1],[1],[0]],[[-1],[-1],[1],[0]],[[-1],[1],[-1],[0]],[[-1],[-1],[-1],[0]]])
			# eightVertices = np.ones((8,4))

			rc = rand_color(agent.id)

			vertices = []
			for i in range(8):
				offset = bbox_vector * mask[i]
				vertex = loc + offset

				# print (vertex)

				vertex = np.dot(Rt.matrix, vertex)
				trnasformed_3d_pos = np.dot(inv(extrinsic.matrix), vertex)
				pos2d = np.dot(k, trnasformed_3d_pos[:3])

				pos2d = np.array([pos2d[0] / pos2d[2], pos2d[1] / pos2d[2], pos2d[2]])
				# print (pos2d)
				if pos2d[2] > 0:
					x_2d = int(WINDOW_WIDTH - pos2d[0])
					y_2d = int(WINDOW_HEIGHT - pos2d[1])
					if (x_2d >= 0 and x_2d < WINDOW_WIDTH and y_2d >= 0 and y_2d < WINDOW_HEIGHT):
						# print (x_2d, y_2d)
						vertices.append([x_2d, y_2d])
						# bbox_image[y_2d, x_2d] = 0

			vertices = np.array(vertices)
			if vertices.shape[0] > 0:
				# print (vertices.shape)
				x_min = vertices[:,0].min()
				x_max = vertices[:,0].max()
				y_min = vertices[:,1].min()
				y_max = vertices[:,1].max()
				# bbox_image[x_min:x_max, y_min:y_max] = 0
				bbox_image[y_min:y_max, x_min:x_max] = 0
						# draw_rect(x_2d, y_2d, rgb_image)
	# plt.imshow(bbox_image)
	# plt.show()
	return bbox_image
	

class ImitationLearning(object):
	# origin: 115,510
	def __init__(self, camera_to_car_transform, lidar_to_car_transform, image_cut=[38, 170]):
		self.dropout_vec = [1.0] * 8 + [0.7] * 2 + [0.5] * 2 + [0.5] * 1 + [0.5, 1.] * 5

		self.learning_rate = 1e-5
		self.steer_loss_weight = 1
		self.throttle_loss_weight = 0.01
		self.brake_loss_weight = 0.01
		
		self._image_size = (88, 200, 3)
		self._camera_to_car_transform = camera_to_car_transform
		self._lidar_to_car_transform = lidar_to_car_transform

		# config_gpu = tf.ConfigProto()
		# config_gpu.gpu_options.visible_device_list = '0'
		# config_gpu.gpu_options.per_process_gpu_memory_fraction = 0.25
		# self._sess = tf.Session(config=config_gpu)
		self._sess = tf.Session()

		with tf.device('/cpu:0'):
			# with tf.device('/gpu:0'):
			self._input_images = tf.placeholder("float", shape=[None, self._image_size[0], self._image_size[1], self._image_size[2]], name="input_image")

			self._input_data = []

			self._input_data.append(tf.placeholder(tf.float32, shape=[None, 4], name="input_control"))

			self._input_data.append(tf.placeholder(tf.float32, shape=[None, 1], name="input_speed"))

			self._dout = tf.placeholder("float", shape=[len(self.dropout_vec)])

			### hxc
			self._y = tf.placeholder(tf.float32, shape=[None, 3])

			self._input_depth = tf.placeholder("float", shape=[None, self._image_size[0], self._image_size[1], 1], name="input_depth")

			### hxc
			# self._direction = tf.placeholder(tf.float32)
			# self._mask = tf.placeholder(tf.float32, shape=[None, 5])
			

		with tf.name_scope("Network"):
			# self._network_tensor, self._solvers, self._losses = load_imitation_learning_network(self._input_images, self._y, self._input_data, self._image_size, self._dout)
			self._network_tensor = load_imitation_learning_network(self._input_images, self._input_data, self._image_size, self._dout, self._input_depth)
			
			# print (self._network_tensor)
			# print (self._mask)
			# self._all_net = tf.boolean_mask(tensor=self._network_tensor, mask=self._mask)

		import os
		dir_path = os.path.dirname(__file__)

		self._models_path = dir_path + './model/'

		# tf.reset_default_graph()
		self._sess.run(tf.global_variables_initializer())

		self.load_model()
		self.saver = tf.train.Saver()


		temp = set(tf.global_variables())
		# print (len(temp))

		self._losses, self._solvers = self.add_loss_opt(self._network_tensor)

		# print (len(set(tf.global_variables())))
		self._sess.run(tf.variables_initializer(set(tf.global_variables()) - temp))
		# self._sess.run(tf.global_variables_initializer())
        



		self._image_cut = image_cut

	def add_loss_opt(self, network_tensor):
		losses = []
		solvers = []
		for i in range(len(network_tensor)-1):
			branch = network_tensor[i]
			# print (branch)
			loss = tf.reduce_mean(self.steer_loss_weight*tf.squared_difference(branch[0][0], self._y[0][0]) + \
											self.throttle_loss_weight*tf.squared_difference(branch[0][1], self._y[0][1]) + \
											self.brake_loss_weight*tf.squared_difference(branch[0][2], self._y[0][2]))

			losses.append(loss)

			sol = tf.train.AdamOptimizer(self.learning_rate).minimize(loss)
			solvers.append(sol)	

		return losses, solvers


	def save_model(self):
		save_path = self.saver.save(self._sess, 'our_model/model.ckpt')

	def load(self):
		self.saver.restore(self._sess, 'our_model/model.ckpt')

	def load_model(self):

		variables_to_restore = tf.global_variables()

		# print (len(variables_to_restore))
		for i in range(len(variables_to_restore)):
			print (i, variables_to_restore[i])

		pretrained_vars = variables_to_restore[:10] + variables_to_restore[20:]
		saver = tf.train.Saver(pretrained_vars, max_to_keep=0)

		if not os.path.exists(self._models_path):
			raise RuntimeError('failed to find the models path')

		ckpt = tf.train.get_checkpoint_state(self._models_path)
		if ckpt:
			print('Restoring from ', ckpt.model_checkpoint_path)
			saver.restore(self._sess, ckpt.model_checkpoint_path)
		else:
			ckpt = 0

		return ckpt

	def train(self, batch):
		for direction in DIRECTIONS:
			rgb_image = batch[direction]['rgb']
			depth_image = batch[direction]['depth']
			measurements = batch[direction]['measure']

			rgb_image = np.array(rgb_image)
			
			# print (rgb_image.shape)
			rgb_image = rgb_image[:,self._image_cut[0]:self._image_cut[1],:,:]
			# print (rgb_image.shape)
			img_batch = [ scipy.misc.imresize(rgb_image[i], [self._image_size[0], self._image_size[1]]) for i in range(rgb_image.shape[0])]
			rgb_image = np.array(img_batch)
			# print (rgb_image.shape)

			images_aug = augment.augment_images(rgb_image)
			# print (images_aug.shape)
			image_input = np.squeeze(images_aug)
			# plt.imshow(images_aug)
			# plt.show()
			image_input = image_input.astype(np.float32)
			image_input = np.multiply(image_input, 1.0 / 255.0)
		

			# depth
			depth_image = np.array(depth_image)

			depth_image = depth_image[:,self._image_cut[0]:self._image_cut[1],:]
			# print (depth_image.shape)

			depth_batch = [cv2.resize(depth_image[i], dsize=(200, 88), interpolation=cv2.INTER_CUBIC) for i in range(depth_image.shape[0])]

			depth_image = np.expand_dims(depth_batch, axis=3)
			# print ('depth ...')
			# print (depth_image.shape)
			# print (depth_image.shape)

			# semantic_image = sensor_data['CameraSemantic0'].data[self._image_cut[0]:self._image_cut[1], :]
			# # plt.imshow(semantic_image)
			# # plt.show()
			# semantic_image = np.expand_dims(semantic_image, axis=2)


			# image_input = np.concatenate((image_input, depth_image), axis=3)
			# print (image_input.shape)

			# processing bbox and lidar
			'''
			bbox_images = []
			lidar_images = []
			for m in range(len(measurements)):
				bb = get_bounding_box(measurements[m], self._camera_to_car_transform, self._lidar_to_car_transform, batch[direction]['rgb'][m])
				bb = bb[self._image_cut[0]:self._image_cut[1],:]
				bbox_images.append(cv2.resize(bb, dsize=(200, 88), interpolation=cv2.INTER_CUBIC))
				
				ll = bb[self._image_cut[0]:self._image_cut[1],:]
				ll = get_lidar(measurements[m], self._camera_to_car_transform, self._lidar_to_car_transform, batch[direction]['rgb'][m], batch[direction]['lidar'][m])
				lidar_images.append(cv2.resize(ll, dsize=(200, 88), interpolation=cv2.INTER_CUBIC))

			bbox_images = np.array(bbox_images)
			lidar_images = np.array(lidar_images)
			bbox_images = np.expand_dims(bbox_images, axis=3)
			lidar_images = np.expand_dims(lidar_images, axis=3)
			'''
			# print (bbox_images.shape, lidar_images.shape)
			

			# net
			branches = self._network_tensor
			solvers = self._solvers
			losses = self._losses

			x = self._input_images
			dep = self._input_depth

			dout = self._dout
			input_speed = self._input_data[1]
			y = self._y


			# input data
			speed = [j.player_measurements.forward_speed for j in measurements]
			# speed = measurements.player_measurements.forward_speed
			# image_input = image_input.reshape((1, self._image_size[0], self._image_size[1], self._image_size[2]))

			# Normalize with the maximum speed from the training set ( 90 km/h)
			# logger.info(speed)
			speed = np.array(speed) / 30.0
			speed = speed.reshape((BATCH_SIZE, 1))
			# print (speed.shape)

			# ctr = measurements.player_measurements.autopilot_control
			ctr = [[j.player_measurements.autopilot_control.steer, j.player_measurements.autopilot_control.throttle, j.player_measurements.autopilot_control.brake] for j in measurements]
			ctr = np.array(ctr)
			# print (ctr.shape)
			# y_val = np.zeros(3)
			# y_val[0] = ctr.steer
			# y_val[1] = ctr.throttle
			# y_val[2] = ctr.brake
			# y_val = y_val.reshape((1,3))

			if direction == 2 or direction == 0.0:
				# print ('yes')
				all_net = branches[0]
				solver = solvers[0]
				loss = losses[0]

			elif direction == 3:
				all_net = branches[2]
				solver = solvers[2]
				loss = losses[2]

			elif direction == 4:
				all_net = branches[3]
				solver = solvers[3]
				loss = losses[3]

			else:
				all_net = branches[1]
				solver = solvers[1]
				loss = losses[1]

			feedDict = {x: image_input, input_speed: speed, dout: [1] * len(self.dropout_vec), y: ctr, dep: depth_image}

			final_cost = 0
			for i in range(ITERS):
				_, output_all, cost = self._sess.run([solver, branches, loss], feed_dict = feedDict)
				# print (output_all[0][0])
				# print (ctr)
				# logger.info(cost)
				# print ('...')
				final_cost = cost

			# global training_cost
			# training_cost.append(final_cost)
			# predicted_steers = (output_all[0][0])
			# predicted_acc = (output_all[0][1])
			# predicted_brake = (output_all[0][2])
			# # print (predicted_steers, predicted_acc, predicted_brake, cost)
			# final_cost = cost
			# print (cost)


		# training_cost.append(final_cost)

	def predict(self, measurements, sensor_data, direction):

		# print ('testing step...')
		# print ('direction')
		# print (direction)
		# rgb
		
		rgb_image = sensor_data['CameraRGB1'].data[self._image_cut[0]:self._image_cut[1],:]
		plt.imshow(rgb_image)
		plt.show()

		rgb_image = scipy.misc.imresize(rgb_image, [self._image_size[0], self._image_size[1]])
		
		image_input = rgb_image.astype(np.float32)
		image_input = np.multiply(image_input, 1.0 / 255.0)
		# print (sensor_data['CameraRGB1'].data.shape)
		# depth
		# print (sensor_data['CameraDepth1'].data.shape)
		depth_image = sensor_data['CameraDepth1'].data[self._image_cut[0]:self._image_cut[1], :]
		# plt.imshow(sensor_data['CameraDepth1'].data)
		# plt.show()
		# dep_img = depth_to_array(sensor_data['CameraDepth1'])
		plt.imshow(depth_image)
		plt.show()

		depth_image = np.expand_dims(depth_image, axis=2)
		depth_image = cv2.resize(depth_image, dsize=(200, 88), interpolation=cv2.INTER_CUBIC)

		
		depth_image = np.expand_dims(depth_image, axis=2)
		# print (depth_image.shape)
		# depth_image = scipy.misc.imresize(depth_image, [self._image_size[0], self._image_size[1]])

		# semantic_image = sensor_data['CameraSemantic0'].data[self._image_cut[0]:self._image_cut[1], :]
		# # plt.imshow(semantic_image)
		# # plt.show()
		# semantic_image = np.expand_dims(semantic_image, axis=2)

		# image_input = np.concatenate((image_input, depth_image), axis=2)
		# print (image_input.shape)
		
		# lidar_image = get_lidar(measurements, self._camera_to_car_transform, self._lidar_to_car_transform, sensor_data['CameraRGB1'].data, sensor_data['Lidar32'].data)

		# processing bbox and lidar
		'''
		bbox_image = get_bounding_box(measurements, self._camera_to_car_transform, self._lidar_to_car_transform, sensor_data['CameraRGB1'].data)
		bbox_image = bbox_image[self._image_cut[0]:self._image_cut[1],:]
		bbox_image = cv2.resize(bbox_image, dsize=(200, 88), interpolation=cv2.INTER_CUBIC)
		
		lidar_image = get_lidar(measurements, self._camera_to_car_transform, self._lidar_to_car_transform, sensor_data['CameraRGB1'].data, sensor_data['Lidar32'].data)
		lidar_image = lidar_image[self._image_cut[0]:self._image_cut[1],:]
		lidar_image = cv2.resize(lidar_image, dsize=(200, 88), interpolation=cv2.INTER_CUBIC)

		bbox_image = np.expand_dims(bbox_image, axis=0)
		lidar_image = np.expand_dims(lidar_image, axis=0)
		bbox_image = np.expand_dims(bbox_image, axis=3)
		lidar_image = np.expand_dims(lidar_image, axis=3)
		'''
		# print (bbox_image.shape, lidar_image.shape)
		

		# net
		branches = self._network_tensor
		x = self._input_images
		dep = self._input_depth
		dout = self._dout
		input_speed = self._input_data[1]
		y = self._y

		# input data
		speed = measurements.player_measurements.forward_speed
		image_input = image_input.reshape((1, self._image_size[0], self._image_size[1], self._image_size[2]))


		# Normalize with the maximum speed from the training set ( 90 km/h)
		speed = np.array(speed / 30.0)
		speed = speed.reshape((1, 1))

		ctr = measurements.player_measurements.autopilot_control
		y_val = np.zeros(3)
		y_val[0] = ctr.steer
		y_val[1] = ctr.throttle
		y_val[2] = ctr.brake
		y_val = y_val.reshape((1,3))

		if direction == 2 or direction == 0.0:
			# print ('yes')
			all_net = branches[0]
		elif direction == 3:
			all_net = branches[2]
		elif direction == 4:
			all_net = branches[3]
		else:
			all_net = branches[1]

		# print (all_net[0])

		# self._sess.run(tf.global_variables_initializer())
		# print (np.expand_dims(depth_image, axis=0).shape)
		feedDict = {x: image_input, input_speed: speed, dout: [1] * len(self.dropout_vec), y: y_val, dep: np.expand_dims(depth_image, axis=0)}
		# feedDict = {x: image_input, input_speed: speed, dout: [1] * len(self.dropout_vec), y: y_val, dep: lidar_image}
		
		# print ('loss......')
		# print (ctr)

		output_all = self._sess.run([all_net], feed_dict = feedDict)
		# print (output_all[0][0])
		# print (y_val[0])
		cost = (self.steer_loss_weight*output_all[0][0][0]-y_val[0][0])**2 + self.throttle_loss_weight*(output_all[0][0][1]-y_val[0][1])**2 + self.brake_loss_weight*(output_all[0][0][2]-y_val[0][2])**2
		testing_cost.append(cost)
		# logger.info(output_all)
		# logger.info(y_val)
		# logger.info('...')
		# predicted_steers = (output_all[0][0])
		# predicted_acc = (output_all[0][1])
		# predicted_brake = (output_all[0][2])
		# print (predicted_steers, predicted_acc, predicted_brake, cost)


datasetFilesTrain = glob.glob('/cluster/scratch/hx/new_out/_out/'+'*.pkl')
# datasetFilesTrain = glob.glob('../../data/_out/'+'*.pkl')


def read(net):
	batch = defaultdict(lambda: defaultdict(list))
	# print (datasetFilesTrain)
	# print (len(datasetFilesTrain))
	
	random.shuffle(datasetFilesTrain)
	# print (datasetFilesTrain)

	# for filename in glob.glob(folder+'*.pkl'):
	for filename in datasetFilesTrain:
		with open(filename, 'rb') as f:
			# print (filename)
			x = pickle.load(f)
			lis = random.sample(range(275), FRAMES)
			for i in lis:
				img = x['sensor_data'][i]
				measurements = carla_protocol.Measurements()
				measurements.ParseFromString(x["measurements"][i])

				direction = x['commands'][i]

				if direction == 0.0:
					direction = 2
				if len(batch[direction]['rgb']) < BATCH_SIZE:
					batch[direction]['rgb'].append(img['CameraRGB1'].data)
					batch[direction]['depth'].append(img['CameraDepth1'].data)
					batch[direction]['measure'].append(measurements)
					batch[direction]['lidar'].append(img['Lidar32'].data)

	# for i in DIRECTIONS:
	# 	print (len(batch[i]['rgb']))
	net.train(batch)

def test(folder, net):
	# logger.info('testing...')
	for filename in glob.glob(folder+'*.pkl'):
		# logger.info(filename)

		with open(filename, 'rb') as f:
			x = pickle.load(f)
			# 275 images per episode
			for i in range(FRAMES):
				img = x['sensor_data'][i]
				measurements = carla_protocol.Measurements()
				measurements.ParseFromString(x["measurements"][i])
				direction = x['commands'][i]
				net.predict(measurements, img, direction)

	# global testing_cost
	# logger.info('test loss: ')
	# logger.info(np.mean(testing_cost))
	# testing_cost = []

def main():
	camera_to_car_transform, lidar_to_car_transform = get_camera_params()
	net = ImitationLearning(camera_to_car_transform, lidar_to_car_transform) # first

	
	# read('./_out/', net)
	# read('/cluster/scratch/hx/_out/', net)


	# net.save_model()
	# net.load()	# second


	# test('./_out/', net)
	# test('/cluster/scratch/hx/_out/', net)

	# net.predict(measurements, sensor_data, direction) # third in the loop

	for i in range(EPOCHS):
		# logger.info('training...')
		# read(net)
		# net.save_model()
		# net.load()
		test('../../data/_test/', net)


		# train on cluster
		# logger.info('training...')
		# read(net)
		# net.save_model()
		# net.load()
		# test('/cluster/scratch/hx/_test/', net)


if __name__ == '__main__':
    main()










