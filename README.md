# 3D Vision Project: CARLA
- Group members: Xingchang Huang, Qi Dai, Haoran Wang （Group 12）
- Supervisor: Zhaopeng Cui
- [Report](https://gitlab.ethz.ch/3D-Driver/Carla-Imitation-Learning/blob/master/report.pdf)
- [Gitlab repository](https://gitlab.ethz.ch/3D-Driver/Carla-Imitation-Learning/edit/master/)

## Prerequisite
- Carla 0.8.1
- numpy
- scipy
- PIL
- Python 3.6.4
- Tensorflow 1.8.0
- Imgaug Library


## Running
1. Run CARLA server following the documents [here](https://carla.readthedocs.io/en/latest/running_simulator_standalone/).
2. Generate dataset using accodring to the instructions in [data_preparation](data_preparation/data_preparation.md)
3. Train our agent on our created dataset using scripts in ```./network_training/imitation```
4. Test our agent CARLA simulator using scripts in ```./testing```



