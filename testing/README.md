# Test our Autonomous Driving Model
Python scripts in this folder are intended to test our autonomous driving model, and output some performance indicators.

## Prerequisite
- Carla 0.8.1
- numpy
- scipy
- Tensorflow
- PIL

Note that A carla server should also run at the same time. 

Model parameter should be provided in advance. In our cases parameter are saved in folder _./agents/our_depth_model_

## Running
Basically run

$ python 3d_run_benchmark.py

If you want to test in Town 2, run

$ python 3d_run_benchmark.py --city-name Town02

Note that the carla server should also run in Town2 mode.



