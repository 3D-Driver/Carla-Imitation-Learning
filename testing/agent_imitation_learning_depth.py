'''
The agent for testing the trained model
NOTE: 
- the setting of image_cut
- the speed is scaled by 90
- update the saved model in folder './model'
'''
from __future__ import print_function

import os

import scipy

import tensorflow as tf
import numpy as np
import pickle
from numpy.linalg import pinv, inv
import math

slim = tf.contrib.slim

from carla.agent import Agent
from carla.carla_server_pb2 import Control
from imitation_learning_network_depth import load_imitation_learning_network
from carla.transform import Transform

WINDOW_WIDTH = 267
WINDOW_HEIGHT = 200
WINDOW_WIDTH_HALF = WINDOW_WIDTH / 2
WINDOW_HEIGHT_HALF = WINDOW_HEIGHT / 2

k = np.identity(3)
k[0, 2] = WINDOW_WIDTH_HALF
k[1, 2] = WINDOW_HEIGHT_HALF
k[0, 0] = k[1, 1] = WINDOW_WIDTH / (2.0 * math.tan(100.0 * math.pi / 360.0))

def get_camera_params():
    camera_to_car_transform =  np.matrix([[-5.91458986e-17, 2.58819045e-01, 9.65925826e-01, 2.00000000e+00],
                                [-1.00000000e+00, 3.74939946e-33, -6.12323400e-17, 0.00000000e+00],
                                [ 1.58480958e-17, 9.65925826e-01, -2.58819045e-01, 1.40000000e+00],
                                [ 0.00000000e+00, 0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

    lidar_to_car_transform = np.matrix([[ 6.123234e-17,-1.000000e+00, 0.000000e+00, 0.000000e+00],
                                [ 1.000000e+00, 6.123234e-17, 0.000000e+00, 0.000000e+00],
                                [ 0.000000e+00, 0.000000e+00, -1.000000e+00, 2.500000e+00],
                                [ 0.000000e+00, 0.000000e+00, 0.000000e+00, 1.000000e+00]])

    return camera_to_car_transform, lidar_to_car_transform

def get_lidar(measurements, camera_to_car_transform, lidar_to_car_transform, lidar_data):
	lidar_image = np.ones((WINDOW_HEIGHT, WINDOW_WIDTH))

	# print (lidar_data.shape)
	for i in range(lidar_data.shape[0]):
		pos_vector = np.array([ [lidar_data[i,0]], [lidar_data[i,1]], [lidar_data[i,2]], [1.0]]) # homo.
		# print (pos_vector)
		if pos_vector[1] < 0:
			point_pos = np.dot(lidar_to_car_transform, pos_vector)
			# print (point_pos)
			point_pos = np.dot(inv(camera_to_car_transform), point_pos)
			# print (point_pos)
			pos2d = np.dot(k, point_pos[:3])
			pos2d = np.array([pos2d[0] / pos2d[2], pos2d[1] / pos2d[2], pos2d[2]])
			# print (pos2d)
			x_2d = int(WINDOW_WIDTH - pos2d[0])
			y_2d = int(WINDOW_HEIGHT - pos2d[1])
			# print (x_2d, y_2d)
			if (x_2d >= 0 and x_2d < WINDOW_WIDTH and y_2d >= 0 and y_2d < WINDOW_HEIGHT):
				lidar_image[y_2d, x_2d] = np.sqrt(np.sum(pos_vector**2)) / 1000
	
	return lidar_image

class ImitationLearning(Agent):

    def __init__(self, city_name, avoid_stopping, memory_fraction=0.25, image_cut=[38, 170]):

        Agent.__init__(self)
        self._camera_to_car_transform, self._lidar_to_car_transform = get_camera_params()

        self.dropout_vec = [1.0] * 8 + [0.7] * 2 + [0.5] * 2 + [0.5] * 1 + [0.5, 1.] * 5

        config_gpu = tf.ConfigProto()

        # GPU to be selected, just take zero , select GPU  with CUDA_VISIBLE_DEVICES
        # Below code is the configuration for GPU, 

        # config_gpu.gpu_options.visible_device_list = '0'
        # config_gpu.gpu_options.per_process_gpu_memory_fraction = memory_fraction

        self._image_size = (88, 200, 3)
        self._avoid_stopping = avoid_stopping
        self._depth_size = (88, 200, 1)

        # GPU Version
        # self._sess = tf.Session(config=config_gpu)
        self._sess = tf.Session()

        # with tf.device('/gpu:0'):
        with tf.device('/cpu:0'):
            self._input_images = tf.placeholder("float", shape=[None, self._image_size[0],
                                                                self._image_size[1],
                                                                self._image_size[2]],
                                                name="input_image")

            self._input_data = []
            
            # I'm not sure wht should be put into the "input_control"
            self._input_data.append(tf.placeholder(tf.float32,
                                                   shape=[None, 4], name="input_control"))

            self._input_data.append(tf.placeholder(tf.float32,
                                                   shape=[None, 1], name="input_speed"))

            self._dout = tf.placeholder("float", shape=[len(self.dropout_vec)])

            self._input_depth = tf.placeholder("float", shape=[None, self._depth_size[0],
                                                                self._depth_size[1],
                                                                self._depth_size[2]],
                                                                name="input_depth")
            
            self._y = tf.placeholder(tf.float32, shape=[None, 3])

        with tf.name_scope("Network"):
            self._network_tensor = load_imitation_learning_network(self._input_images,
                                                                   self._input_data,
                                                                   self._image_size, 
                                                                   self._dout, self._input_depth)
        import os
        dir_path = os.path.dirname(__file__)

        self._models_path = dir_path + '/agents/our_depth_model/'

        # tf.reset_default_graph()
        self._sess.run(tf.global_variables_initializer())

        self.load_model()

        self._image_cut = image_cut

    def load_model(self):

        variables_to_restore = tf.global_variables()

        saver = tf.train.Saver(variables_to_restore, max_to_keep=0)

        if not os.path.exists(self._models_path):
            raise RuntimeError('failed to find the models path')

        ckpt = tf.train.get_checkpoint_state(self._models_path)
        if ckpt:
            print('Restoring from ', ckpt.model_checkpoint_path)
            saver.restore(self._sess, ckpt.model_checkpoint_path)
        else:
            ckpt = 0

        return ckpt

    def add_loss_opt(self, network_tensor):
        losses = []
        solvers = []
        for i in range(len(network_tensor)-1):
            branch = network_tensor[i]
            print (branch)
            loss = tf.reduce_mean(0.45*tf.squared_difference(branch[0][0], self._y[0][0]) + \
											0.45*tf.squared_difference(branch[0][1], self._y[0][1]) + \
											0.05*tf.squared_difference(branch[0][2], self._y[0][2]))
                                            
            losses.append(loss)
            sol = tf.train.AdamOptimizer(0.0002).minimize(loss)
            solvers.append(sol)
            
        return losses, solvers


    def run_step(self, measurements, sensor_data, directions, target):

        '''Prepossess Image'''
        img_rgb = sensor_data['CameraRGB1'].data[self._image_cut[0]:self._image_cut[1], :, :]
        img_rgb = scipy.misc.imresize(img_rgb, [self._image_size[0], self._image_size[1]])
        img_rgb = img_rgb.astype(np.float32)
        img_rgb = np.multiply(img_rgb, 1.0 / 255.0)
        '''
        lidar_image = get_lidar(measurements, self._camera_to_car_transform, self._lidar_to_car_transform, sensor_data['Lidar32'].data)
        lidar_image = lidar_image[self._image_cut[0]:self._image_cut[1],:]
        img_depth = scipy.misc.imresize(lidar_image, [self._image_size[0], self._image_size[1]])
        img_depth = np.expand_dims(img_depth, axis=2)
        '''
        img_depth = sensor_data['CameraDepth1'].data[self._image_cut[0]:self._image_cut[1], :]
        img_depth = scipy.misc.imresize(img_depth, [self._depth_size[0], self._depth_size[1]])

        img_depth = np.expand_dims(img_depth, axis=2)
        

        control = self._compute_action(img_rgb, img_depth, 
                                measurements.player_measurements.forward_speed, directions)

        return control

    def _compute_action(self, img_rgb, depth, speed, direction=None):
        # Note: img_rgbd has already been cut to (88, 200, 4)

        # rgb_image = rgb_image[self._image_cut[0]:self._image_cut[1], :]

        # image_input = scipy.misc.imresize(rgb_image, [self._image_size[0], self._image_size[1]])

        # image_input = image_input.astype(np.float32)
        # image_input = np.multiply(image_input, 1.0 / 255.0)
        steer, acc, brake = self._control_function(img_rgb, depth, speed, direction, self._sess)

        # This a bit biased, but is to avoid fake breaking

        if brake < 0.1:
            brake = 0.0

        if acc > brake:
            brake = 0.0

        # We limit speed to 35 km/h to avoid
        if speed > 10.0 and brake == 0.0:
            acc = 0.0

        control = Control()
        control.steer = steer
        control.throttle = acc
        control.brake = brake

        control.hand_brake = 0
        control.reverse = 0

        return control

    def _control_function(self, image_input, depth, speed, control_input, sess):

        branches = self._network_tensor
        x = self._input_images
        dout = self._dout
        input_speed = self._input_data[1]
        dep = self._input_depth

        image_input = image_input.reshape(
            (1, self._image_size[0], self._image_size[1], self._image_size[2]))

        depth = depth.reshape((1, self._depth_size[0], self._depth_size[1], self._depth_size[2]))

        # print("Shape of RGB: " + str(image_input.shape))
        # print("Shape of Depth: " + str(depth.shape))

        # Normalize with the maximum speed from the training set (90 km/h)
        speed = np.array(speed / 90.0)

        speed = speed.reshape((1, 1))

        if control_input == 2 or control_input == 0.0:
            all_net = branches[0]
        elif control_input == 3:
            all_net = branches[2]
        elif control_input == 4:
            all_net = branches[3]
        else:
            all_net = branches[1]

        feedDict = {x: image_input, input_speed: speed, dout: [1] * len(self.dropout_vec), dep: depth}

        output_all = sess.run(all_net, feed_dict=feedDict)

        predicted_steers = (output_all[0][0])

        predicted_acc = (output_all[0][1])

        predicted_brake = (output_all[0][2])

        if self._avoid_stopping:
            predicted_speed = sess.run(branches[4], feed_dict=feedDict)
            predicted_speed = predicted_speed[0][0]
            real_speed = speed *90.0

            real_predicted = predicted_speed * 90.0
            if real_speed < 2.0 and real_predicted > 3.0:
                # If (Car Stooped) and
                #  ( It should not have stopped, use the speed prediction branch for that)

                predicted_acc = 1 * (5.6 /90.0- speed) + predicted_acc

                predicted_brake = 0.0

                predicted_acc = predicted_acc[0][0]

        return predicted_steers, predicted_acc, predicted_brake
