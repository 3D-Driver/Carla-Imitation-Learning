## Data preparation
1. copy [client_datareader.py](data_preparation/client_datareader.py) to the `/path/to/CARLA/PythonClient`
2. lanunch CARLA in server mode by running `./CarlaUE4.sh -carla-server`
3. Generate data by running `python client_datareader.py` or `python small_client_datareader.py`  